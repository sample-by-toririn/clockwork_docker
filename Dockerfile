FROM ruby:2.6.3-slim-stretch
ENV LANG C.UTF-8
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN bundle install

CMD ["bundle", "exec", "clockwork", "config/clock.rb"]
