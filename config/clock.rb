# 定期実行で利用するクラスの読み込み
require_relative '../lib/clock_test'

# Clockworkを利用するためにモジュールを読み込み
include Clockwork
# 定期実行で利用するクラスの読み込み
include ClockTest

# Clockworkの設定値
configure do |config|
  # 日本時間のタイムゾーンを設定
  config[:tz] = "Asia/Tokyo"
end

# 毎日朝09:00に実行
every(1.day, 'morning', at: '09:00') do
  Foo.new.run
end

# 毎日お昼13:00に実行
every(1.day, 'noon', at: '13:00') do
  Foo.new.run
end

# 毎日夜19:00に実行
every(1.day, 'night', at: '19:00') do
  Foo.new.run
end

# 3秒に1回実行
every(3.seconds, "test") do
  Foo.new.run
end
