# 初期化処理の読み込み
require_relative "./clock_test/initialize"
# 全ファイルの読み込み
Dir[File.dirname(__FILE__) + '/clock_test/**/*.rb'].each {|file| require file }

module ClockTest
end
