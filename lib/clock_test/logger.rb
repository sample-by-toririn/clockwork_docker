require "logger"
module ClockTest
  # Loggerのラッパークラス
  class Logger
    # ログをローテートするサイズ
    LOG_ROTATE_SIZE = 10 * 1024 * 1024
    # ログを保持するファイルの最大数
    MAX_LOG_FILE_NUM = 5

    # 情報レベルのログを書き込む
    def self.info(log)
      outputs(log)
      logger.info(log)
    end

    # 警告レベルのログを書き込む
    def self.warn(log)
      outputs(log)
      logger.warn(log)
    end

    # エラーレベルのログを書き込む
    def self.error(log)
      outputs(log)
      logger.error(log)
    end

    private

    def self.outputs(log)
      puts log
    end

    def self.logger
      @@logger ||= ::Logger.new("./log.log", MAX_LOG_FILE_NUM, LOG_ROTATE_SIZE)
    end
  end
end
