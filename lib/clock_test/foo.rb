require_relative "logger"
module ClockTest
  class Foo
    def run
      current_time = Time.new.strftime("%Y年%m月%d日の%H時%M分%S秒だよ！")
      Logger.info(current_time)
    end
  end
end
